﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibCalculatingCircleArea
{
    public static class CalculateCircleArea
    {
        public static double Calculate(double R)
        {
            return Math.Round(Math.PI * Math.Pow(R, 2), 3);
        }
    }
}
