﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ClassLibCalculatingCircleArea;

namespace UnitTestCalculatingCircleArea
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethodCalculateCircleArea()
        {
            double R = 4.5;
            double result = 63.617;

            var actual = CalculateCircleArea.Calculate(R);

            Assert.AreEqual(result, actual);
        }
    }
}
