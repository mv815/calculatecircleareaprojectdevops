﻿using ClassLibCalculatingCircleArea;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppCalculatingCircleArea
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        double R;
        double result;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonCalculate_Click(object sender, RoutedEventArgs e)
        {
            R = Double.Parse(TextBoxRadius.Text, CultureInfo.InvariantCulture);
            result = CalculateCircleArea.Calculate(R);
            TextBoxResult.Text = result.ToString();
        }

        private void TextBoxRadius_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBoxResult.Text = "";
        }
    }
}
